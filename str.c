#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"str.h"

str str_create(char*p)
{
	str s={
		.size=0,
		.ptr=NULL,
	};

	str_assign(&s,p);
	return s;
}

void str_free(str*s)
{
	if(!s->ptr)
		return;

	free(s->ptr);
	s->ptr=NULL;
}

void str_grow(str*s,size_t newsize)
{
	if(!s)
	{
		printf("error: cannot assign to NULL str\n");
		return;
	}

	if(!s->ptr)
	{
		s->ptr=malloc(newsize+1);
		if(!s->ptr)
		{
			printf("error: could not allocate str\n");
			s->size=0;
			return;
		}
	}

	else
	{
		s->ptr=realloc(s->ptr,newsize+1);
		if(!s->ptr)
		{
			printf("error: could not reallocate str\n");
			s->size=0;
			return;
		}
	}

	s->ptr[s->size]='\0';
	s->size=newsize;
}

void str_assign(str*s,char*p)
{
	if(!s)
	{
		printf("error: cannot assign to NULL str\n");
		return;
	}

	str_grow(s,strlen(p));
	strcpy(s->ptr,p);
	s->size=strlen(p);
}

void str_concat(str*s,char*p)
{
	if(!s)
	{
		printf("error: cannot assign to NULL str\n");
		return;
	}

	str_grow(s,s->size+strlen(p));
	strcat(s->ptr,p);
}

int str_ws(str*s)
{
	if(!s)
	{
		printf("error: cannot assign to NULL str\n");
		return;
	}
	return strpbrk(s->ptr," \t")!=NULL;
}
