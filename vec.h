#pragma once
#include"str.h"

#define VEC_BLOCK_SIZE 256

typedef struct vec
{
	size_t size;
	size_t cap;
	str*ptr;
} vec;

// Initialize vec
vec vec_create(void);

// Grow vec
void vec_grow(vec*v);

// Free internal memory
void vec_free(vec*v);

// Add a new element
void vec_push(vec*v,str s);

// Return last element and remove it
str vec_pop(vec*v);

// Return last element without removing
str vec_peek_last(vec*v);

// Sort by initial ASCII character
void vec_sort_ascii(vec*v);

// Return longest element
str vec_longest(vec*v);

// Return shortest element
str vec_shortest(vec*v);

// Calculate total length of all strs using space
size_t vec_total_length(vec*v,size_t space);

// Use qsort
void vec_sort_qsort(vec*v);
