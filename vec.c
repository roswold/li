#include"vec.h"
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<strings.h>
#include<ctype.h>

vec vec_create(void)
{
	vec v={
		.ptr=NULL,
		.size=0,
		.cap=0,
	};

	vec_grow(&v);
	return v;
}

str vec_longest(vec*v)
{
	str s={.size=0,.ptr=NULL};
	if(!v)
	{
		puts("error: cannot search NULL vec");
		return s;
	}

	for(size_t i=0;i<v->size;++i)
		if(v->ptr[i].size>s.size)
			s=v->ptr[i];
	return s;
}

str vec_shortest(vec*v)
{
	str s={.size=0,.ptr=NULL};
	if(!v)
	{
		puts("error: cannot search NULL vec");
		return s;
	}

	for(size_t i=0;i<v->size;++i)
		if(v->ptr[i].size<s.size)
			s=v->ptr[i];
	return s;
}

size_t vec_total_length(vec*v,size_t space)
{
	size_t len=0;
	for(size_t i=0;i<v->size;++i)
	{
		len+=v->ptr[i].size;
		if(i+1<v->size)
			len+=space;
	}

	return len;
}

void vec_sort_ascii(vec*v)
{
	str tmp;

	for(size_t i=0;i<v->size;++i)
		for(size_t j=i+1;j<v->size;++j)
			//if(tolower(v->ptr[i].ptr[0])>tolower(v->ptr[j].ptr[0]))
			if( strcasecmp( (v->ptr[i].ptr), (v->ptr[j].ptr) ) > 0 )
			{
				tmp=v->ptr[i];
				v->ptr[i]=v->ptr[j];
				v->ptr[j]=tmp;
			}
}

void vec_push(vec*v,str s)
{
	if(!v)
	{
		puts("error: cannot push to NULL vec!");
		return;
	}

	if(! v->size+1<v->cap)
		vec_grow(v);

	if(v)
		v->ptr[v->size++]=s;
}

void vec_grow(vec*v)
{
	if(!v)
	{
		puts("error: cannot grow NULL vec!");
		return;
	}

	if(v->cap==0)
	{
		v->ptr=malloc(VEC_BLOCK_SIZE*sizeof(str));
		if(!v->ptr)
		{
			puts("error: failed to allocate vec");
			return;
		}
		v->cap=VEC_BLOCK_SIZE;
	}

	else if(v->cap>0)
	{
		v->ptr=realloc(v->ptr,v->cap+VEC_BLOCK_SIZE*sizeof(str));
		if(!v->ptr)
		{
			puts("error: failed to reallocate vec");
			return;
		}
		v->cap+=VEC_BLOCK_SIZE;
	}
}

void vec_free(vec*v)
{
	if(v)
	{
		if(v->ptr)
		{
			for(size_t i=0;i<v->size;++i)
				if(v->ptr[i].ptr)
					str_free(v->ptr+i);
			free(v->ptr);
		}
		v->size=0;
		v->cap=0;
	}
}

str vec_pop(vec*v)
{
	str s={
		.ptr=NULL,
		.size=0,
	};

	if(!v)
	{
		puts("error: cannot pop NULL vec!");
		return s;
	}

	if(v->size<1)
		return s;

	// Create new str, copy from last element, free element
	s=str_create(v->ptr[v->size-1].ptr);
	str_free(v->ptr+(v->size-1));
	--v->size;
	return s;
}

str vec_peek_last(vec*v)
{
	str s={
		.ptr=NULL,
		.size=0,
	};

	if(!v)
	{
		puts("error: cannot pop NULL vec!");
		return s;
	}

	if(v->size<1)
		return s;

	return v->ptr[v->size-1];
}

int vec_sort_cmp(const void*a,const void*b)
{
	str*sa=(str*)a;
	str*sb=(str*)b;

	return strcasecmp( sa->ptr, sb->ptr );
}

void vec_sort_qsort(vec*v)
{
	qsort(v->ptr,v->size,sizeof(str),vec_sort_cmp);
}
