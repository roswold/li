#include <dirent.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <unistd.h>

#include "vec.h"
#include "str.h"

#define C_BLUE		"\033\1331;94m"
#define C_CYAN		"\033\1331;36m"
#define C_GREEN		"\033\1331;32m"
#define C_PURPLE	"\033\1331;35m"
#define C_RESET		"\033\1331;0m"
#define C_WHITE		"\033\1331;97m"
#define C_YELLOW	"\033\1331;33m"

void printdir(const char*path,int wide,int showhidden,int color,int sort);

int main(int argc,char**argv)
{
	char*helpmsg="usage: li [-1GUal] [--help] [DIRECTORIES]";
	int color=false;
	int showhidden=false;
	int sort=true;
	int wide=true;
	vec directories;

	directories=vec_create();

	// Parse arguments
	for(int i=1;i<argc;++i)
	{
		// Long options
		if(strstr(argv[i],"--")==argv[i])
		{
			if(strcmp(argv[i],"--help")==0)
			{
				puts(helpmsg);
				goto quit;
			}
			else
			{
				printf("error: unrecognized long option '%s'\n",argv[i]);
				goto quit;
			}
		}

		// Short options
		if(argv[i][0]=='-')
		{
			if(strcmp(argv[i],"-")==0)
			{
				printf("error: expected short option\n");
				goto quit;
			}

			for(int j=1;argv[i][j];++j)
			{
				switch(argv[i][j])
				{
					case 'G': color=true; break;
					case 'U': sort=false; break;
					case 'a': showhidden=true; break;
					case '1':
					case 'l': wide=false; break;
					default: printf("error: unrecognized short option '-%c'\n",argv[i][j]); goto quit;
				}
			}
		}

		else
			vec_push(&directories,str_create(argv[i]));
	}

	// Don't display color codes or spacing if used in a pipe
	if(!isatty(1))
	{
		color=false;
		wide=false;
	}

	// Assume current working directory in leiu of arguments
	if(directories.size<1)
		vec_push(&directories,str_create("."));

	// Print directories
	for(int i=0;i<directories.size;++i)
	{
		if(directories.size>1)printf("%s:\n",directories.ptr[i].ptr);
		printdir(directories.ptr[i].ptr,wide,showhidden,color,sort);
	}

quit:
	vec_free(&directories);
}


void printdir(const char*path,int wide,int showhidden,int color,int sort)
{
	DIR *dir;
	int first=true;
	int space=2;
	int columns=0;
	int term_height;
	int term_width;
	int width=0;
	str longest;
	struct dirent*pdirent;
	struct stat buf;
	struct winsize win_size;
	vec v;

	v=vec_create();

	// Get terminal width
    ioctl(0, TIOCGWINSZ, &win_size);
	term_width=win_size.ws_col;
	term_height=win_size.ws_row;

	dir=opendir(path);
	if(!dir)
	{
		printf("error: could not open directory '%s'\n",path);
		return;
	}

	// Collect file names in vec
	while((pdirent=readdir(dir))!=NULL)
	{
		if(pdirent->d_name[0]!='.' || showhidden)
			vec_push(&v,str_create(pdirent->d_name));
	}

	// Prevent divide by zero (and extra CPU cycles)
	if(v.size>0)
	{
		// Sort if requested
		if(sort)
			vec_sort_qsort(&v);

		longest=vec_longest(&v);
		columns=term_width/(longest.size+1);
		space=longest.size+1;
	}

	// Print names out
	for(size_t i=0;i<v.size;++i)
	{

		// Hide dot files
		int stat_success;
		int is_dir=false;
		int is_exec=false;
		int is_blk=false;
		int is_link=false;
		int is_sock=false;
		str fullpath;

		// Determine if directory
		fullpath=str_create((char*)path);
		str_concat(&fullpath,"/");
		str_concat(&fullpath,v.ptr[i].ptr);
		stat_success=lstat(fullpath.ptr,&buf);
		if(strcmp(v.ptr[i].ptr,".")==0 ||
				strcmp(v.ptr[i].ptr,"..")==0)
			is_dir=true;
		else
		{
			if(stat_success)printf("error: could not stat '%s'\n",
					v.ptr[i].ptr);
			is_dir=S_ISDIR(buf.st_mode);
		}

		// Determine if executable
		is_exec=buf.st_mode&S_IEXEC;

		// Determine if block or character device
		is_blk=S_ISBLK(buf.st_mode) || S_ISCHR(buf.st_mode);

		// Determine if link
		is_link=S_ISLNK(buf.st_mode);

		// Determine if socket
		is_sock=S_ISSOCK(buf.st_mode);

		// Break line depending on terminal geometry
		//if(wide && width+space+strlen(v.ptr[i].ptr)>columns*space)
		if(wide && width+space>columns*space)
		{
			printf("\n");
			width=0;
		}

		//else if(wide && !first)
			//width+=printf("  ");
		if(first)first=false;

		if(columns>=v.size)
		{
			if(color)
				printf("%s",C_RESET);
			if(wide && width>0)
				width+=printf("  ");

			// Colorize
			if(color)
			{
				if(is_dir)			printf("%s",C_BLUE);
				else if(is_link)	printf("%s",C_CYAN);
				else if(is_sock)	printf("%s",C_PURPLE);
				else if(is_exec)	printf("%s",C_GREEN);
				else if(is_blk)		printf("%s",C_YELLOW);
				else				printf("%s",C_RESET);
			}

			if(str_ws(v.ptr+i))
				width+=printf("'%s'",v.ptr[i].ptr);
			else
				width+=printf("%s",v.ptr[i].ptr);
			width+=printf("%s",wide?"":"\n");

			//width+=printf("%-2s%s",
					//v.ptr[i].ptr,
					//wide?"  ":"\n");
		}

		else
		{
			size_t goal=width+space;

			// Colorize
			if(color)
			{
				if(is_dir)			printf("%s",C_BLUE);
				else if(is_link)	printf("%s",C_CYAN);
				else if(is_sock)	printf("%s",C_PURPLE);
				else if(is_blk)		printf("%s",C_YELLOW);
				else if(is_exec)	printf("%s",C_GREEN);
				else				printf("%s",C_RESET);
			}

			//width+=printf("%s",v.ptr[i].ptr);
			if(str_ws(v.ptr+i))
				width+=printf("'%s'",v.ptr[i].ptr);
			else
				width+=printf("%s",v.ptr[i].ptr);

			if(color)
				printf("%s",C_RESET);

			while(width<goal)
			{
				putchar(' ');
				++width;
			}

			if(!wide)
			{
				putchar('\n');
				//++width;
			}

			// Print file name
			//width+=printf("%-*s%s",
					//space,
					//v.ptr[i].ptr,
					//wide?"":"\n");
		}

		if(color)
			printf("%s",C_RESET);

		str_free(&fullpath);
	}

	if(wide && v.size>0)
		printf("\n");

	vec_free(&v);
}
